$:.unshift(File.expand_path(File.join(File.dirname(__FILE__), "lib")))
require "simulator"

Simulator::Service.run! :port => Simulator.config[:port], :host => Simulator.config[:host]