# frozen_string_literal: true

# load message handler monads stuffs in /handlers/monads folder
Dir["#{File.dirname(__FILE__)}/handlers/monads/**.rb"]
  .each { |file| require_relative file }

# load message handler component stuffs in /handlers/component folder
Dir["#{File.dirname(__FILE__)}/handlers/component/**.rb"]
  .each { |file| require_relative file }

require_relative 'handlers/tasks/base'
require_relative 'handlers/tasks/step'
require_relative 'handlers/tasks/pass'
require_relative 'handlers/tasks/fail'
require_relative 'handlers/tasks/when_truthy'
require_relative 'handlers/tasks/when_falsy'

## require base handler class
require_relative 'handlers/base'

# load message handler stuffs in /handlers folder
Dir["#{File.dirname(__FILE__)}/handlers/**_handler.rb"]
  .each { |file| require_relative file }
