# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      # retrieve configs handler
      class ConfigHandler < Base
        pass :retrieve_config

        def retrieve_config(context)
          context.result[:config] = {
            service: Simulator.config.to_hash,
          }
        end
      end
    end
  end
end
