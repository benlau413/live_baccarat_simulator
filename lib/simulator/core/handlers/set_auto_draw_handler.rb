# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      class SetAutoDrawHandler < Base
        include Component::AutoDrawShoeCommon

        pass :set_auto_draw,             params: [:message]
        pass :compose_result,            params: [:auto_draw]

        def compose_result(context, auto_draw:)
          context.result[:auto_draw] = auto_draw
        end
      end
    end
  end
end
