# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      class ShoeShowCardsHandler < Base
        include Component::NormalCommon

        pass :compose_shoe_result!,                                       params: [:message]
        pass :get_total_given_card_and_counting_card_hash_to_result!,     params: []

      end
    end
  end
end
