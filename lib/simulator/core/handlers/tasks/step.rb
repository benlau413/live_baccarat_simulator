# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      module Tasks
        class Step < Base
          def call(runner, context)
            super ? true : false
          end
        end
      end
    end
  end
end
