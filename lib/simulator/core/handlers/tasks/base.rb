# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      module Tasks
        class Base
          attr_reader :method
          attr_reader :options

          def initialize(method, options)
            @method  = method
            @options = options
            @params  = Array(options[:params]) || []
          end

          def call(runner, context)
            if (params = extract_params(@params, context)).empty?
              runner.send(method, context)
            else
              runner.send(method, context, params)
            end
          end

          def type
            @type ||= self.class.name.to_s.split(':').last
          end

          def to_hash
            {
              type: type,
              method: method,
              options: options
            }
          end

          alias to_h to_hash

          protected

          def extract_params(params, context)
            params.each_with_object({}) do |key, result|
              sym_key = key.to_sym
              data = context[sym_key]
              result[sym_key] = data unless data.nil?
            end
          end
        end
      end
    end
  end
end
