# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      module Tasks
        class WhenTruthy < Step
          class InvalidHaltArgumentError < Simulator::Errors::UnexpectedError
            def initialize(method)
              super("step #{method} require halt method.")
            end
          end

          attr_reader :halt

          def initialize(method, options)
            super
            @halt = options[:halt]
            @halt_params = Array(options[:halt_params]) || []

            raise InvalidHaltArgumentError, method unless @halt
          end

          def call(runner, context)
            @output = super
            if condition?
              raise InvalidHaltArgumentError, halt unless runner.respond_to?(halt)

              if (params = extract_params(@halt_params, context)).empty?
                runner.send(halt, context)
              else
                runner.send(halt, context, params)
              end

              context.halt
            end

            true
          end

          protected

          def condition?
            @output
          end
        end
      end
    end
  end
end
