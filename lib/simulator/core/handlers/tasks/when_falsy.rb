# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      module Tasks
        class WhenFalsy < WhenTruthy
          protected

          def condition?
            !super
          end
        end
      end
    end
  end
end
