# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      module Tasks
        class Fail < Base
          def call(runner, context)
            super
            false
          end
        end
      end
    end
  end
end
