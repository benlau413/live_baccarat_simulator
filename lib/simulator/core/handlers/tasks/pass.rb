# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      module Tasks
        class Pass < Base
          def call(runner, context)
            super
            true
          end
        end
      end
    end
  end
end
