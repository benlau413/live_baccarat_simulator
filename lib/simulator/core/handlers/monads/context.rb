# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      module Monads
        class Context
          DEFAULT_SUCCESS_CODE = 'OK'

          extend Forwardable

          def_delegators :@ctx, :[], :[]=, :has_key?

          attr_reader :message
          attr_reader :result

          attr_accessor :error

          def initialize(msg)
            @message = msg
            reset
          end

          def success?
            !failure?
          end

          def failure?
            !@error.nil?
          end

          def halted?
            @halted
          end

          def halt
            @halted = true
          end

          def output
            failure? ? error : result
          end

          protected

          def reset
            @result = {
              code: DEFAULT_SUCCESS_CODE
            }
            @ctx = {
              message: @message,
              result: @result
            }
            @halted = false
            @error = nil
          end
        end
      end
    end
  end
end
