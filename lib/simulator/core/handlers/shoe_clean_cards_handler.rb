# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      class ShoeCleanCardsHandler < Base
        include Component::NormalCommon

        pass :call_cards_group_clean,             params: [:message]
        pass :compose_shoe_result!,               params: [:message]

        def call_cards_group_clean(context, message:)
          Component::ShoeActionCommon.cards_group_clean(message.shoe_box_id)
        end

      end
    end
  end
end
