# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      class ShoeInputCardsHandler < Base
        include Component::NormalCommon

        pass :call_cards_group_input!,            params: [:message]
        pass :compose_shoe_result!,               params: [:message]

        def call_cards_group_input!(context, message:)
          call_cards_group_input(message.shoe_box_id, message.cards)
        end
      end
    end
  end
end
