# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      class ShoeActionHandler < Base

        pass :call_shoe_action_common,                   params: [:message]

        def call_shoe_action_common(context, message:)
          draw_result = Component::ShoeActionCommon.process(action: message.name.to_s, shoe_box_id: message.shoe_box_id, card: message.card)
          context[:result].merge!(draw_result)
        end
      end
    end
  end
end
