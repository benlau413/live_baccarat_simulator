# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      class ShoeShowAllHandler < Base
        include Component::NormalCommon

        pass :compose_show_shoe_all_result!,                              params: [:message]
        pass :get_total_given_card_and_counting_card_hash_to_result!,     params: []

        def compose_show_shoe_all_result!(context, message:)
          shoe = Component::ShoeActionCommon.show_shoe_arrays
          context[:result].merge!(
            shoe: shoe
          )
        end
      end
    end
  end
end
