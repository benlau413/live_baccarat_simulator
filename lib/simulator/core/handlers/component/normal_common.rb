# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      module Component
        module NormalCommon
          def unlock_shoe_action(shoe_box_id)
            Component::ShoeActionCommon.process(action: Component::ShoeActionCommon::UNLOCK_SHOE, shoe_box_id: shoe_box_id)
          end

          def lock_shoe_action(shoe_box_id)
            Component::ShoeActionCommon.process(action: Component::ShoeActionCommon::LOCK_SHOE, shoe_box_id: shoe_box_id)
          end

          def draw_shoe_action(shoe_box_id, card = nil)
            card = Component::ShoeActionCommon.get_card_from_cards_group(shoe_box_id)
            Component::ShoeActionCommon.process(action: Component::ShoeActionCommon::DRAW_CARD, shoe_box_id: shoe_box_id, card: card)
          end

          def draw_shoe_by_input_option(shoe_box_id, card)
            Component::ShoeActionCommon.process(action: Component::ShoeActionCommon::DRAW_CARD, shoe_box_id: shoe_box_id, card: card)
          end

          def call_cards_group_input(shoe_box_id, cards)
            Component::ShoeActionCommon.cards_group_input(shoe_box_id, cards)
          end

          def compose_notify_result!(context, message:)
            context[:result].merge!(message)
          end

          def compose_shoe_result!(context, message:)
            shoe = Component::ShoeActionCommon.show_shoe_arrays(message.shoe_box_id)
            context[:result].merge!(
              shoe_box_id: message.shoe_box_id,
              shoe: shoe
            )
          end

          def get_total_given_card_and_counting_card_hash_to_result!(context)
            value_hash = Component::ShoeActionCommon.get_total_given_card_and_counting_card_hash
            context[:result].merge!(
              total_given_card: value_hash[:total_given_card],
              counting_card_hash: value_hash[:counting_card_hash]
            )
          end
        end
      end
    end
  end
end