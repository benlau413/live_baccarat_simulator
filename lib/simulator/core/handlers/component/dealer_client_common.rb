# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      module Component
        module DealerClientCommon
          def send_result_to_dealer_client(params)
            ws_response = Simulator::Requesters::WsCenter.dealer_client_call(params)
          end

          def notify_handler_send_result_to_dealer_client(context, result:)
            params = build_base_params(result)
            Simulator.logger.info("send_dealer_client_message: #{params}")
            ws_response = send_result_to_dealer_client(params)
          end

          def notify_draw_card_result_handler_send_result_to_dealer_client(context, result:)
            params = build_base_params(result)
            params.merge!("card": result[:card])
            if result[:action] == "notify_burn_card_result"
              %i[burn_cards total_count remain_count].each do | key |
                params.merge!(
                  key => result[key.to_sym]
                )
              end
            elsif result[:action] == "notify_deal_card_result"
              %i[player_cards banker_cards black_card].each do | key |
                params.merge!(
                  key => result[key.to_sym]
                )
              end
            end

            Simulator.logger.info("send_dealer_client_draw_card_message: #{params}")

            ws_response = send_result_to_dealer_client(params)
          end

          def notify_resulted_send_result_to_dealer_client(context, result:)
            params = build_base_params(result)
            %i[player_cards banker_cards player_point banker_point winner pair natural super6].each do | key |
              params.merge!(
                key => result[key.to_sym]
              )
            end
            ws_response = send_result_to_dealer_client(params)
          end

          def build_base_params(result)
            params = {
              "action": result[:action],
              "game_id": result[:game_id],
              "round_id": result[:round_id],
              "feed_id": result[:feed_id],
              "shoe_id": result[:shoe_id],
              "hand": result[:hand],
              "state": result[:state]
            }
          end
        end
      end
    end
  end
end