# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      module Component
        class ShoeActionCommon
          SUIT_LOOKUP = ['c', 'd', 'h', 's']
          FACE_VALUE = ['A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K']
          BLACK_CARD = "black_card"
          TOTAL_CARD_GROUP = 8
          UNLOCK_SHOE = "unlock_shoe"
          LOCK_SHOE = "lock_shoe"
          DRAW_CARD = "draw_card"

          $cards_group = {}
          $total_given_card = 0
          $counting_card_hash = {}

          class << self
            ## cards group method
            def cards_group(shoe_box_id)
              $cards_group[shoe_box_id] ||= []
            end

            def cards_group_clean(shoe_box_id)
              cards_group(shoe_box_id).clear
            end

            def cards_group_input(shoe_box_id, cards)
              cards.each do |card|
                cards_group(shoe_box_id) << card
              end
            end

            def get_card_from_cards_group(shoe_box_id)
              cards_group(shoe_box_id).shift
            end

            def show_shoe_arrays(shoe_box_id = nil)
              if shoe_box_id
                cards_group(shoe_box_id)
              else
                $cards_group
              end
            end

            ## main process
            def process(action:, shoe_box_id:, card: nil)
              kafka_message = build_kafka_message(action, shoe_box_id, card)
              init_counting_card_hash(kafka_message)
              kafka_info = kafka_produce_message(kafka_message)
              compose_shoe_action_result(kafka_message, kafka_info)
            end


            def build_kafka_message(action, shoe_box_id, card)
              kafka_message = {
                action: action,
                shoe_box_id: shoe_box_id
              }

              if kafka_message[:action] == DRAW_CARD
                kafka_message.merge!(
                  'card': generate_card(card)
                )
              end
              kafka_message
            end


            ## for init card group
            def init_counting_card_hash(kafka_message)
              if kafka_message[:action] == UNLOCK_SHOE || kafka_message[:action] == LOCK_SHOE      ## whether shoe unlock or lock can init card_hash
                $total_given_card = 0
                $counting_card_hash = Hash.new
                SUIT_LOOKUP.each do |suit|
                  $counting_card_hash.merge!(
                    suit => value_hash
                  )
                end
              end
            end

            def value_hash
              value_hash = Hash.new()
              FACE_VALUE.each do |value|
                value_hash.merge!(
                  value => 0
                )
              end
              value_hash
            end


            ## for generate card
            def generate_card(message_card)
              if message_card
                card = message_card if message_card == BLACK_CARD
                unless message_card == BLACK_CARD
                  face_value = message_card[0]
                  suit_lookup = message_card[1]
                  raise Errors::WrongCardMessageError.new("card #{message_card} is a wrong card standard.") unless SUIT_LOOKUP.include?(suit_lookup) && FACE_VALUE.include?(face_value)
                end
              else
                if trigger_black_card
                  card = BLACK_CARD
                else
                  while 1 do
                    suit_lookup = SUIT_LOOKUP[draw_number(SUIT_LOOKUP.size)]
                    face_value = FACE_VALUE[draw_number(FACE_VALUE.size)]
                    break if $counting_card_hash[suit_lookup][face_value] < TOTAL_CARD_GROUP
                  end
                end
              end
              card = after_draw_card_handling(suit_lookup, face_value) unless card == BLACK_CARD
              card
            end

            def trigger_black_card
              num = draw_number(TOTAL_CARD_GROUP*52 - 20)
              # if $total_given_card - 50 > num
              if $total_given_card == 350
                $total_given_card += 1
                return true
              else
                return false
              end
            end

            def draw_number(array_size)
              RandomKit::GamingRandom::PRNG.rand( 0...array_size )
            end

            def after_draw_card_handling(suit_lookup, face_value)
              raise Errors::TotalCardNumberLimitError.new("Total Card number has already reached or over maximum #{52 * TOTAL_CARD_GROUP} cards") if $total_given_card >= 52 * TOTAL_CARD_GROUP
              raise Errors::CardExceedLimitError.new("Card #{face_value + suit_lookup} has already reached maximum #{TOTAL_CARD_GROUP} cards") if $counting_card_hash[suit_lookup][face_value] >= TOTAL_CARD_GROUP
              $total_given_card += 1
              $counting_card_hash[suit_lookup][face_value] += 1
              card = face_value + suit_lookup
            end



            def kafka_produce_message(kafka_message)
              sleep 1
              kafka_info = Simulator::Requesters::MessageCenter.kafka_produce_message(kafka_message)
            end

            def get_total_given_card_and_counting_card_hash
              value_hash = {
                total_given_card: $total_given_card,
                counting_card_hash: $counting_card_hash
              }
            end

          ## for result
            def compose_shoe_action_result(kafka_message, kafka_info)
              result = {
                action: kafka_message[:action],
                kafka_message: kafka_message,
                kafka_info: kafka_info,
              }
              result.merge!(
                total_given_card: $total_given_card,
                counting_card_hash: $counting_card_hash
              ) if kafka_message[:action] == DRAW_CARD
              result
            end
          end
        end
      end
    end
  end
end