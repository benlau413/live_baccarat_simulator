# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      module Component
        module AutoDrawShoeCommon

          def set_auto_draw(context, message:)
            $auto_draw = message.auto_draw
            context[:auto_draw] = $auto_draw
          end

          def call_shoe_draw_card(context, message:)
            if $auto_draw         ## when auto_draw = true, auto draw a card, otherwise wait for api call
              draw_result = draw_shoe_action(message.feed_id)
              Simulator.logger.info("draw_result = #{draw_result}")
              draw_result
            end
          end

          def call_shoe_unlock_and_lock(context, message:)
            if $auto_draw
              unlock_result = unlock_shoe_action(message.feed_id)
              Simulator.logger.info("unlock_result = #{unlock_result}")
              sleep 2
              lock_result = lock_shoe_action(message.feed_id)
              Simulator.logger.info("lock_result = #{lock_result}")
            end
          end
        end
      end
    end
  end
end