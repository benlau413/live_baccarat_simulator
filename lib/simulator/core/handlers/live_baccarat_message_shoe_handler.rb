# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      class LiveBaccaratMessageShoeHandler < Base
        include Component::NormalCommon

        FACE_VALUE = Component::ShoeActionCommon::FACE_VALUE
        pass :get_shoe_message!,                  params: [:message]
        pass :handle_shoe_message,                params: [:shoe_message]

        def get_shoe_message!(context, message:)
          context[:shoe_message] = message.message
        end

        def handle_shoe_message(context, shoe_message:)
          case shoe_message["action"]
          when "shuffle_end"
            unlock_shoe_action(shoe_message[:shoe_box_id])
          when "shuffle_begin"
            lock_shoe_action(shoe_message[:shoe_box_id])
          when "burn_card", "game_card"
            raise Simulator::Errors::ParameterMismatchError.new("Real Shoe message draw card action without card param") if shoe_message[:card].nil?
            card_number = shoe_message[:card][0].to_i * 10 + shoe_message[:card][1].to_i
            our_card = FACE_VALUE[card_number - 1] + shoe_message[:card][2]
            draw_shoe_by_input_option(shoe_message[:shoe_box_id], our_card)
          when "cut_card"
            our_card = "black_card"
            draw_shoe_by_input_option(shoe_message[:shoe_box_id], our_card)
          end
        end
      end
    end
  end
end
