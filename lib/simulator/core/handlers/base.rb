# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      ## Base Message Handler
      ## Just provide hook method for other handler to inherite
      class Base
        Errors = Simulator::Errors
        Requesters = Simulator::Requesters

        class FailedTask < Simulator::Errors::UnexpectedError
          def initialize(task)
            super("Failed to run #{task.type} method: #{task.method}")
          end
        end

        class << self
          def dispatch(message)
            new.call(message)
          end

          def step(method, **options)
            step_tasks << Tasks::Step.new(method, options)
          end

          def pass(method, **options)
            step_tasks << Tasks::Pass.new(method, options)
          end

          def fail(method, **options)
            fail_tasks << Tasks::Fail.new(method, options)
          end

          def when_falsy(method, **options)
            step_tasks << Tasks::WhenFalsy.new(method, options)
          end

          def when_truthy(method, **options)
            step_tasks << Tasks::WhenTruthy.new(method, options)
          end

          def final(method, **options)
            final_tasks << Tasks::Pass.new(method, options)
          end

          def step_tasks
            @step_tasks ||= []
          end

          def fail_tasks
            @fail_tasks ||= []
          end

          def final_tasks
            @final_tasks ||= []
          end

          def extract_name
            name.underscore.split('/').last.gsub(/_handler/, '')
          end
        end

        def call(message)
          context = create_context(message)
          run_step_tasks(context)

          unless context.halted?
            run_fail_tasks(context) if context.failure?
          end

          context.output
        ensure
          run_final_tasks(context)
        end

        def extract_name
          self.class.extract_name
        end

        protected

        def run_step_tasks(context)
          self.class.step_tasks.each do |task|
            run_step_task(task, context)
            break if context.halted? || context.failure?
          end
        end

        def run_step_task(task, context)
          run_task(task, context) || raise(FailedTask, task)
        rescue StandardError => ex
          Simulator.log_exception ex
          context.error = ex
          false
        end

        def run_final_tasks(context)
          self.class.final_tasks.each do |task|
            run_task(task, context)
          end
        end

        def run_fail_tasks(context)
          self.class.fail_tasks.each do |task|
            run_task(task, context)
          end
        end

        def run_task(task, context)
          task.call(self, context)
        end

        def create_context(message)
          Monads::Context.new(message)
        end
      end
    end
  end
end
