# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      class NotifyDrawCardHandler < Base
        include Component::AutoDrawShoeCommon
        include Component::DealerClientCommon
        include Component::NormalCommon

        pass :call_shoe_draw_card!,                                     params: [:message]
        pass :compose_notify_result!,                                   params: [:message]
        pass :notify_handler_send_result_to_dealer_client,              params: [:result]

        def call_shoe_draw_card!(context, message:)
          call_shoe_draw_card(context, message: message)
        end

      end
    end
  end
end
