# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      # Loop back
      # Just for health check
      class LoopbackHandler < Base
        pass :loop_back

        def loop_back(context)
          context.result[:name] = context.message.name.to_s
        end
      end
    end
  end
end
