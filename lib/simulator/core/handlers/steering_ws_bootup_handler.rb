# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      class SteeringWsBootupHandler < Base
        pass :boot_up_steering_websocket,         params: %i[message]

        def boot_up_steering_websocket(context, message:)
          ws_message = {
            game_id: message.game_id,
            feed_id: message.feed_id
          }
          ws_response = Simulator::Requesters::WsCenter.steering_bootup(ws_message)
          context[:result].merge!(
            ws_boot_up_result: ws_response
          )
        end
      end
    end
  end
end
