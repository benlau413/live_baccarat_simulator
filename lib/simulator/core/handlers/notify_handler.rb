# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      class NotifyHandler < Base
        include Component::DealerClientCommon
        include Component::NormalCommon

        step :compose_notify_result!,                           params: [:message]
        when_truthy :action_is_notify_resulted?,                params: [:result],
                                                                halt: :notify_resulted_send_result_to_dealer_client,  halt_params: [:result]
        pass :notify_handler_send_result_to_dealer_client,      params: [:result]

        def action_is_notify_resulted?(context, result:)
          result[:action] == "notify_resulted"
        end
      end
    end
  end
end
