# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      class OpenFeedResultHandler < Base
        include Component::AutoDrawShoeCommon
        include Component::DealerClientCommon
        include Component::NormalCommon

        pass :compose_notify_result!,                                   params: [:message]
        pass :notify_handler_send_result_to_dealer_client,              params: [:result]
        pass :handle_result_message,                                    params: [:message]

        def call_shoe_draw_card!(context, message:)
          call_shoe_draw_card(context, message: message)
        end

        def handle_result_message(context, message:)
          case message.round_info["state"]
          when "new_shoed" || "burn_started"
            call_shoe_common_init_counting_card_hash
            call_shoe_draw_card()
          end
        end

        def call_shoe_common_init_counting_card_hash
          Component::ShoeActionCommon.init_counting_card_hash
        end
      end
    end
  end
end
