# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      class ShoeDrawHandler < Base
        include Component::NormalCommon

        pass :call_action_common_draw_card,       params: [:message]
        pass :compose_shoe_result!,               params: [:message]

        def call_action_common_draw_card(context, message:)
          draw_result = draw_shoe_action(message.shoe_box_id)
          context[:result].merge!(draw_result)
        end
      end
    end
  end
end
