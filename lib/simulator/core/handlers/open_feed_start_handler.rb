# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      class QueryFeedStartHandler < Base
        include Component::AutoDrawShoeCommon
        step :build_ws_params!,                                 params: [:message]
        pass :send_message_by_ws,                               params: [:ws_message]
        pass :set_auto_draw,                                    params: [:message]
        step :compose_query_feed_start_result!,                  params: %i[message ws_result]

        def build_ws_params!(context, message:)
          context[:ws_message] = {
            "action": "query_feed",
            "feed_id": message.feed_id,
            "game_id": message.game_id.to_i
          }
        end

        def send_message_by_ws(context, ws_message:)
          context[:ws_result] = Simulator::Requesters::WsCenter.steering_call(ws_message)
        end

        def compose_query_feed_start_result!(context, message:, ws_result:)
          context[:result].merge!(
            action: message.action,
            feed_id: message.feed_id,
            game_id: message.game_id,
            ws_result: ws_result,
            auto_draw: message.auto_draw
          )
        end
      end
    end
  end
end
