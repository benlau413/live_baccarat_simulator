# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      class NotifyChangeShoeHandler < Base
        include Component::AutoDrawShoeCommon
        include Component::DealerClientCommon
        include Component::NormalCommon

        step :compose_notify_result!,                           params: [:message]
        pass :notify_handler_send_result_to_dealer_client,      params: [:result]
        pass :call_shoe_unlock_and_lock,                        params: [:message]

      end
    end
  end
end
