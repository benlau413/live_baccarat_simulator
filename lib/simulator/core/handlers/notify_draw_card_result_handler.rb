# frozen_string_literal: true

module Simulator
  module Core
    module Handlers
      class NotifyDrawCardResultHandler < Base
        include Component::AutoDrawShoeCommon
        include Component::DealerClientCommon
        include Component::NormalCommon

        step :compose_notify_result!,                                             params: [:message]
        pass :notify_draw_card_result_handler_send_result_to_dealer_client,       params: [:result]
        when_truthy :continue_draw_card_checking?,                                params: [:message],
                                                                                  halt: :call_shoe_draw_card, halt_params: [:message]

        def continue_draw_card_checking?(context, message:)
          checking = true
          if message.action == "notify_burn_card_result"
            checking = false if message.remain_count == 0
            Simulator.logger.info("\nburn_remain_count: #{message.remain_count}")
          elsif message.action == "notify_deal_card_result"
            checking = false if message.next_deal_index.nil?
            Simulator.logger.info("\ndraw_next_card?: #{!message.next_deal_index.nil?}")
          end
          checking
        end
      end
    end
  end
end
