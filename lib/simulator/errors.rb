# frozen_string_literal: true

## require base error
require_relative 'errors/simulator_error'
## require  common base error
require_relative 'errors/unexpected_error'
require_relative 'errors/client_error'

# load error code stuffs in /errors folder
Dir["#{File.dirname(__FILE__)}/errors/**_error.rb"]
  .each { |file| require_relative file }
