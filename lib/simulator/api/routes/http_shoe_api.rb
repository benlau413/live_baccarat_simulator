# frozen_string_literal: true

module Simulator
  module Api
    module Routes
      ## define http shoe api routes
      module HttpShoeApi
        def self.registered(app)
          app.namespace Simulator.config[:'service.shoe.base_path'].to_s do

            get "/draw/shoe_box/{shoe_box_id}.?:format?" do
              @action = 'shoe_draw'
              process(params, extract_message_data, params.fetch('format', 'json'))
            end

            post "/input_cards/shoe_box/{shoe_box_id}.?:format?" do
              @action = 'shoe_input_cards'
              process(params, extract_message_data, params.fetch('format', 'json'))
            end

            get "/clean_cards/shoe_box/{shoe_box_id}.?:format?" do
              @action = 'shoe_clean_cards'
              process(params, extract_message_data, params.fetch('format', 'json'))
            end

            get "/show_cards/shoe_box/{shoe_box_id}.?:format?" do
              @action = 'shoe_show_cards'
              process(params, extract_message_data, params.fetch('format', 'json'))
            end

            get "/show_cards.?:format?" do
              @action = 'shoe_show_all'
              process(params, extract_message_data, params.fetch('format', 'json'))
            end


            get "/lock/shoe_box/{shoe_box_id}.?:format?" do
              @action = 'lock_shoe'
              process(params, extract_message_data, params.fetch('format', 'json'))
            end

            get "/unlock/shoe_box/{shoe_box_id}.?:format?" do
              @action = 'unlock_shoe'
              process(params, extract_message_data, params.fetch('format', 'json'))
            end

            post "/draw/shoe_box/{shoe_box_id}/by_input.?:format?" do
              @action = 'draw_card'
              process(params, extract_message_data, params.fetch('format', 'json'))
            end

            get "/set/auto_draw/{auto_draw}.?:format?" do
              @action = 'set_auto_draw'
              process(params, extract_message_data, params.fetch('format', 'json'))
            end
          end
        end
      end
    end
  end
end
