# frozen_string_literal: true

module Simulator
  module Api
    module Routes
      ## define http dealer api routes
      module HttpDealerApi
        def self.registered(app)
          app.namespace Simulator.config[:'service.dealer.base_path'].to_s do
            get "/steering_ws_bootup/game/{game_id}/feed/{feed_id}.?:format?" do
              @action = 'steering_ws_bootup'
              process(params, extract_message_data, params.fetch('format', 'json'))
            end

            get "/query_feed/game/{game_id}/feed/{feed_id}.?:format?" do
              @action = 'query_feed_start'
              process(params, extract_message_data, params.fetch('format', 'json'))
            end
          end
        end
      end
    end
  end
end
