module Simulator
  module Api
    module Routes
      ## define ws dealer client routes
      module DealerClientApi
        def self.registered(app)
          app.namespace '' do
            get '/ws/dealer_client' do
              headers 'Access-Control-Allow-Origin' => '*'
              headers 'Access-Control-Allow-Headers' => '*'

              if Faye::WebSocket.websocket?(request.env)
                ws = Faye::WebSocket.new(env, nil, ping: 0.1)

                ws.on :open do |event|
                  Simulator.logger.info 'Socket Client connected'
                  Simulator::Requesters::WsCenter.ws_dealer_client << ws
                end

                ws.on :message do |event|
                  Simulator.logger.info "Local Client message received(#{event.data.size}) #{event.data}"
                  params = MultiJson.load(event.data)
                  @action = params["action"]
                  response = process(params, extract_message_data, params.fetch('format', 'json'))

                  ws.send(response)
                end

                ws.on :close do |event|
                  Simulator.logger.info 'Socket Client disconnected'
                  Simulator::Requesters::WsCenter.ws_dealer_client.delete(ws)
                  ws = nil
                end

                ws.rack_response

              else
                'Hello from WebSocket'
              end
            end
          end
        end
      end
    end
  end
end