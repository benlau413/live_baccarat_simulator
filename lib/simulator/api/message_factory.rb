# frozen_string_literal: true

module Simulator
  module Api
    ## Message Factory for dynamic build message
    class MessageFactory
      ## raise MessageRegisterError when message don't inherit from Message::Base
      class MessageRegisterError < Simulator::Errors::UnexpectedError
        def initialize(message)
          super("[#{message}] Message Register Error.")
        end
      end

      ## raise MessageNotFoundError when message isn't in factory mapper
      class MessageNotFoundError < Simulator::Errors::UnexpectedError
        def initialize(message)
          super("[#{message}] Message Not Found.")
        end
      end

      class << self
        def instance
          @instance ||= new
        end

        def compose(data)
          instance.compose(data)
        end
      end

      def initialize
        reset_register
      end

      def compose(data)
        msg_name = parse_message_name(data).to_sym
        klass = get_message(msg_name)
        raise MessageNotFoundError, msg_name if klass.nil?

        klass.build(data)
      end

      def get_message(type)
        mapper[type.to_sym]
      end

      private

      def mapper
        @mapper ||= {}
      end

      def register(type, klass)
        raise MessageRegisterError, type unless klass <= Messages::Base
        mapper[type.to_sym] = klass
      end

      def reset_register
        register(:loopback,                  Messages::LoopbackMessage)
        register(:config,                    Messages::ConfigMessage)

        register(:steering_ws_bootup,        Messages::SteeringWsBootMessage)
        register(:LiveBaccaratMessage,       Messages::LiveBaccaratMessageShoeMessage)
        default_register_message
      end

      def parse_message_name(data)
        data['action'] || ''
      end

      def default_register_message
        shoe_register_message
        dealer_app_register_message
      end

      def shoe_register_message
        register(:draw_card,                Messages::ShoeMessage)
        register(:lock_shoe,                Messages::ShoeMessage)
        register(:unlock_shoe,              Messages::ShoeMessage)

        register(:shoe_draw,                Messages::ShoeDrawMessage)
        register(:shoe_input_cards,         Messages::ShoeInputCardsMessage)
        register(:shoe_clean_cards,         Messages::ShoeCleanCardsMessage)
        register(:shoe_show_cards,          Messages::ShoeShowCardsMessage)
        register(:shoe_show_all,            Messages::ShoeShowAllMessage)
        register(:set_auto_draw,            Messages::SetAutoDrawMessage)
      end

      def dealer_app_register_message
        register(:query_feed_start,          Messages::QueryFeedStartMessage)
        # register(:open_feed,                Messages::OpenFeedResultMessage)

        register(:notify_change_shoe,       Messages::NotifyChangeShoeMessage)
        register(:notify_shoe_locked,       Messages::NotifyShoeLockedMessage)
        register(:notify_shoe_unlocked,     Messages::NotifyShoeUnlockedMessage)
        register(:notify_new_shoe,          Messages::NotifyNewShoeMessage)
        register(:notify_burn_card,         Messages::NotifyBurnCardMessage)
        register(:notify_burn_card_result,  Messages::NotifyBurnCardResultMessage)
        register(:notify_bet_started,       Messages::NotifyBetMessage)
        register(:notify_bet_closing,       Messages::NotifyBetMessage)
        register(:notify_bet_closed,        Messages::NotifyBetMessage)
        register(:notify_deal_card,         Messages::NotifyDealCardMessage)
        register(:notify_deal_card_result,  Messages::NotifyDealCardResultMessage)
        register(:notify_resulted,          Messages::NotifyResultedMessage)
        register(:notify_closed,            Messages::NotifyClosedMessage)
      end
    end
  end
end
