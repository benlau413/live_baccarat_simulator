# frozen_string_literal: true

module Simulator
  module Api
    ## Response Manager for representative response
    class ResponseManager
      ## raise ResponseRegisterError when responser don't inherit from Representable::Decorator
      class ResponseRegisterError < Simulator::Errors::UnexpectedError
        def initialize(responser)
          super("[#{responser}] Response Register Error.")
        end
      end

      ## raise ResponseNotFoundError when response isn't in manager mapper
      class ResponseNotFoundError < Simulator::Errors::UnexpectedError
        def initialize(responser)
          super("[#{responser}] Response Not Found.")
        end
      end

      class << self
        def instance
          @instance ||= new
        end

        def represent(message, result)
          instance.represent(message, result)
        end
      end

      def initialize
        reset_register
      end

      def represent(message, result)
        msg_name = message.name
        klass = get_response(msg_name)
        raise ResponseNotFoundError, msg_name if klass.nil?
        klass.new(result).to_hash
      end

      def get_response(type)
        mapper[type.to_sym]
      end

      def register(type, klass)
        raise ResponseRegisterError, type unless klass <= Responses::Base

        mapper[type.to_sym] = klass
      end

      private

      def mapper
        @mapper ||= {}
      end

      def reset_register
        register(:loopback,            Responses::LoopbackResponse)
        register(:config,              Responses::ConfigResponse)

        register(:steering_ws_bootup,  Responses::SteeringWsBootupResponse)
        register(:LiveBaccaratMessage, Responses::LiveBaccaratMessageShoeResponse)
        default_register_response
      end

      def default_register_response
        shoe_register_response
        dealer_app_register_response
      end

      def shoe_register_response
        register(:draw_card,                    Responses::ShoeResponse)
        register(:lock_shoe,                    Responses::ShoeResponse)
        register(:unlock_shoe,                  Responses::ShoeResponse)

        register(:shoe_draw,                    Responses::ShoeDrawResponse)
        register(:shoe_input_cards,             Responses::ShoeInputCardsResponse)
        register(:shoe_clean_cards,             Responses::ShoeCleanCardsResponse)
        register(:shoe_show_cards,              Responses::ShoeShowCardsResponse)
        register(:shoe_show_all,                Responses::ShoeShowAllResponse)
        register(:set_auto_draw,                Responses::SetAutoDrawResponse)
      end

      def dealer_app_register_response
        register(:query_feed_start,              Responses::QueryFeedStartResponse)
        # register(:open_feed,                    Responses::OpenFeedResultResponse)

        register(:notify_change_shoe,           Responses::NotifyChangeShoeResponse)
        register(:notify_shoe_locked,           Responses::NotifyShoeLockedResponse)
        register(:notify_shoe_unlocked,         Responses::NotifyShoeUnlockedResponse)
        register(:notify_new_shoe,              Responses::NotifyNewShoeResponse)
        register(:notify_burn_card,             Responses::NotifyBurnCardResponse)
        register(:notify_burn_card_result,      Responses::NotifyBurnCardResultResponse)
        register(:notify_bet_started,           Responses::NotifyBetResponse)
        register(:notify_bet_closing,           Responses::NotifyBetResponse)
        register(:notify_bet_closed,            Responses::NotifyBetResponse)
        register(:notify_deal_card,             Responses::NotifyDealCardResponse)
        register(:notify_deal_card_result,      Responses::NotifyDealCardResultResponse)
        register(:notify_resulted,              Responses::NotifyResultedResponse)
        register(:notify_closed,                Responses::NotifyClosedResponse)
      end
    end
  end
end
