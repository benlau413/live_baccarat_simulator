# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      class NotifyBetMessage < NotifyBaseMessage
        define_rule do
          params do
            optional(:countdown).filled(:int?)
          end
        end
      end
    end
  end
end
