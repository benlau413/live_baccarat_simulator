# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      ## For Get System Config API
      class ConfigMessage < Base
      end
    end
  end
end
