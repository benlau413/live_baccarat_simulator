# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      class QueryFeedStartMessage < Base
        define_rule do
          params do
            required(:action).filled(:str?)
            required(:feed_id).filled(:str?)
            required(:game_id).filled(:str?)
            required(:auto_draw).filled(:bool?)
          end
        end

        def before_validate(data)
          data["auto_draw"] = (data["auto_draw"] == "true")
        end
      end
    end
  end
end
