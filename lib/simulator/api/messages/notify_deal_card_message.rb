# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      class NotifyDealCardMessage < NotifyBaseMessage
        define_rule do
          params do
            required(:next_deal_for).filled(:str?)
            required(:next_deal_index).filled(:int?)
          end
        end
      end
    end
  end
end
