# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      class NotifyBurnCardResultMessage < NotifyBaseMessage
        define_rule do
          params do
            required(:type).filled(:str?)
            optional(:burn_index).filled(:int?)
            required(:card).filled(:str?)
            required(:total_count).filled(:int?)
            required(:remain_count).filled(:int?)
            required(:burn_cards).each(:str?)
          end
        end
      end
    end
  end
end
