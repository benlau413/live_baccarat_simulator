# frozen_string_literal: true

require 'dry-validation'

module Simulator
  module Api
    module Messages
      module Utils
        ## Validation Module For Hash data checking
        ## TODO: To be support versioning, maybe have other module
        module Validation
          def self.extended(descendant)
            descendant.extend ClassMethods
          end

          ## For define rule and validate method
          module ClassMethods
            def define_rule(&block)
              @rule = Class.new(defined_rule, &block).new
            end

            def validate(*data)
              result = rule.call(*data)
              raise Simulator::Errors::ParameterMismatchError, result.errors.to_h unless result.success?

              result.to_h
            end

            def validation_fields
              rule.schema.rules.keys || []
            end

            def rule
              @rule ||= defined_rule.new
            end

            private

            def defined_rule
              return superclass.rule.class if defined?(superclass.rule.class)

              Class.new(Dry::Validation::Contract) do
                params {}
              end
            end
          end
        end
      end
    end
  end
end
