# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      ## For Set Auto Draw API
      class SetAutoDrawMessage < Base
        define_rule do
          params do
            required(:auto_draw).filled(:bool?)
          end
        end

        def before_validate(data)
          data["auto_draw"] = (data["auto_draw"] == "true")
        end
      end
    end
  end
end
