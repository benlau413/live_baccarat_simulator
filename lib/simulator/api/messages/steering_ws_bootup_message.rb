# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      ## For Connect Steering WebSocket
      class SteeringWsBootMessage < Base
        define_rule do
          params do
            required(:feed_id).filled(:str?)
            required(:game_id).filled(:str?)
          end
        end
      end
    end
  end
end
