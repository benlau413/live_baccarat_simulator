# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      class ShoeDrawMessage < Base
        define_rule do
          params do
            required(:shoe_box_id).filled(:str?)
          end
        end
      end
    end
  end
end
