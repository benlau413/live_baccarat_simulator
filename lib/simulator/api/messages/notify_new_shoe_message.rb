# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      class NotifyNewShoeMessage < NotifyBaseMessage

      end
    end
  end
end
