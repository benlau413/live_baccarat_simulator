# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      class NotifyResultedMessage < NotifyBaseMessage
        define_rule do
          params do
            required(:player_cards).each(:str?)
            required(:banker_cards).each(:str?)
            required(:player_point).filled(:int?)
            required(:banker_point).filled(:int?)
            required(:winner).filled(:str?)
            required(:pair).filled(:str?)
            required(:natural).filled(:str?)
            required(:super6).filled(:bool?)
          end
        end
      end
    end
  end
end
