# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      class NotifyBurnCardMessage < NotifyBaseMessage
        define_rule do
          params do
            required(:type).filled(:str?)
            required(:next_burn_index).filled(:int?)
          end
        end
      end
    end
  end
end
