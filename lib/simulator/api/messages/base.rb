# frozen_string_literal: true

require 'date'
require 'dry-initializer'

module Simulator
  module Api
    module Messages
      ## Api Message Base Class
      ## TODO: To be support versioning
      class Base
        extend Utils::Validation

        class << self
          def build(data)
            new(data)
          end

          def extract_name
            name.underscore.split('/').last.gsub(/_message/, '')
          end
        end

        define_rule do
          params do
            required(:host).filled(:str?)
            required(:ip).filled(:str?)
            required(:request_id).filled(:str?)
            required(:name).filled(:str?)
          end
        end

        attr_reader :timestamp

        def initialize(data)
          data['name'] = data['action']

          # end of hook for test
          before_validate(data)
          @valid_data = validate(data)
          init_attr(data)
          extract_data(data)
          @timestamp = generate_timestamp
          @name = data['name'].to_sym
        end

        def to_h
          @valid_data
        end

        def before_validate(data)
        end

        alias to_hash to_h

        protected

        def extract_data(data)
          # #Method hook
        end

        def init_attr(data)
          self.class.validation_fields.each do |field_name|
            define_field_reader(field_name)
            instance_variable_set("@#{field_name}", parse_data(data[field_name.to_s]))
          end
        end

        private

        def validate(data)
          self.class.validate(data)
        end

        def generate_timestamp
          (Time.now.to_f * 1000).to_i
        end

        def extract_name
          self.class.extract_name
        end

        def define_field_reader(field_name)
          instance_eval <<-RUBY, __FILE__, __LINE__ + 1
            def #{field_name}           # def default
              @#{field_name}            #   @default
            end                         # end
          RUBY
        end

        def parse_data(data)
          result = (
            case data
            when Array
              data.map { |value| parse_data(value) }
            when Hash # TODO: after remove recursive-open-struct
              RecursiveOpenStruct.new(data, recurse_over_arrays: true)
            else # #String, Numeric, true, false, nil, other case
              data
            end).freeze
          result
        end
      end
    end
  end
end
