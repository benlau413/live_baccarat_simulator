# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      class NotifyDealCardResultMessage < NotifyBaseMessage
        define_rule do
          params do
            required(:card).filled(:str?)
            required(:player_cards).each(:str?)
            required(:banker_cards).each(:str?)
            required(:black_card).filled(:bool?)
            required(:deal_for).filled(:str?)
            required(:deal_index).filled(:int?)
            optional(:next_deal_for).filled(:str?)
            optional(:next_deal_index).filled(:int?)
          end
        end
      end
    end
  end
end
