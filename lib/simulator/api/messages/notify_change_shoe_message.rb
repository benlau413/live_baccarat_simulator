# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      class NotifyChangeShoeMessage < NotifyBaseMessage
        define_rule do
          params do
            optional(:create_at).filled(:str?)
          end
        end
      end
    end
  end
end
