# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      class NotifyShoeLockedMessage < NotifyBaseMessage
        define_rule do
          params do
            required(:shoe_status).filled(:str?)
          end
        end
      end
    end
  end
end
