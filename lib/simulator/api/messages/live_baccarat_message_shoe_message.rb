# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      class LiveBaccaratMessageShoeMessage < Base
        define_rule do
          params do
            required(:action).filled(:str?)
            required(:id).filled(:str?)
            required(:message).hash do
              required(:action).filled(:str?)
              required(:shoe_box_id).filled(:str?)
              optional(:card).filled(:str?)
              optional(:card_left).filled(:int?)
            end
          end
        end
      end
    end
  end
end
