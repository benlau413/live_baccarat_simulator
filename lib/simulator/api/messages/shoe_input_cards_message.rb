# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      class ShoeInputCardsMessage < Base
        define_rule do
          params do
            required(:shoe_box_id).filled(:str?)
            required(:cards).each(:str?)
          end
        end
      end
    end
  end
end
