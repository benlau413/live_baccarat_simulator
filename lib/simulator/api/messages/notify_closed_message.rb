# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      class NotifyClosedMessage < Base
        define_rule do
          params do
            required(:action).filled(:str?)
            required(:feed_id).filled(:str?)
            optional(:shoe_id).filled(:str?)
          end
        end
      end
    end
  end
end
