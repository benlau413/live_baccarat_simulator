# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      ## For System Check API
      class LoopbackMessage < Base
      end
    end
  end
end
