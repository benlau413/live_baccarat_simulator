# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      class NotifyBaseMessage < Base
        define_rule do
          params do
            required(:action).filled(:str?)
            required(:game_id).filled(:int?)
            required(:feed_id).filled(:str?)
            required(:shoe_id).filled(:str?)
            required(:round_id).filled(:str?)
            required(:state).filled(:str?)
            required(:hand).filled(:int?)
            required(:seq_id).filled(:int?)
            required(:ts).filled(:int?)
            required(:request_id).filled(:str?)
          end
        end
      end
    end
  end
end
