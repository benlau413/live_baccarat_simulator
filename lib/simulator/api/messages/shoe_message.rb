# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      ## For Shoe Draw Shoe API
      class ShoeMessage < Base
        define_rule do
          params do
            required(:shoe_box_id).filled(:str?)
            optional(:card).filled(:str?)
          end
        end
      end
    end
  end
end
