# frozen_string_literal: true

module Simulator
  module Api
    module Messages
      class OpenFeedResultMessage < Base
        define_rule do
          params do
            required(:code).filled(:str?)
            required(:feed_id).filled(:str?)
            required(:action).filled(:str?)
            required(:state).filled(:str?)
            required(:request_id).filled(:str?)
            required(:shoe_info).hash do
              required(:lock_status).filled(:str?)
              required(:shoe_id).filled(:str?)
            end
            required(:round_info).hash do
              required(:round_id).filled(:str?)
              required(:seq_id).filled(:int?)
              required(:hand).filled(:int?)
              required(:state).filled(:str?)
              required(:result).maybe(:hash?)
              required(:black_card).filled(:bool?)
              required(:player_cards).maybe(:array?)
              required(:banker_cards).maybe(:array?)
              required(:histories).maybe(:array?)
            end
          end
        end
      end
    end
  end
end
