module Simulator::Api
  class Service < ::Sinatra::Base
    Faye::WebSocket.load_adapter('thin')

    class << self

      def setup_service_type
        create_routes
      end

      def create_routes
        service_types.each do |type|
          klass = Simulator::Api::Routes.const_get(type.camelize)
          register klass
        end
      end

      private

      def service_types
        Simulator.config[:support_service_type]
      end
    end

    setup_service_type
  end
end