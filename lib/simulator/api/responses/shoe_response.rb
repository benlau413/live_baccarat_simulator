# frozen_string_literal: true

module Simulator
  module Api
    module Responses
      # Loop back Api Response
      class ShoeResponse < Base

        property :action,             coerce: String
        property :kafka_message do
          property :shoe_box_id,      coerce: String
          property :card,             coerce: String
        end
        property :kafka_info do
          property :kafka_host,       coerce: String
          property :topic,            coerce: String
        end
        property :total_given_card,   coerce: Integer
        property :counting_card_hash, coerce: Hash
      end
    end
  end
end
