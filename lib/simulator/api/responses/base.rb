# frozen_string_literal: true

module Simulator
  module Api
    module Responses
      # Api Response Base Class
      class Base < Hashie::Trash
        include Hashie::Extensions::IgnoreUndeclared
        include Hashie::Extensions::Dash::Coercion

        property :code,     coerce: String
      end
    end
  end
end
