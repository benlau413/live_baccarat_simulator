# frozen_string_literal: true

module Simulator
  module Api
    module Responses
      class ShoeShowAllResponse < Base

        property :action,         coerce: String
        property :shoe,           coerce: Hash
        property :total_given_card,   coerce: Integer
        property :counting_card_hash, coerce: Hash

      end
    end
  end
end
