# frozen_string_literal: true

module Simulator
  module Api
    module Responses
      class SteeringWsBootupResponse < Base
        property :ws_boot_up_result,  coerce: Common::Boolean
      end
    end
  end
end
