# frozen_string_literal: true

module Simulator
  module Api
    module Responses
      class NotifyDealCardResultResponse < Base
        property :action,         coerce: String
        property :feed_id,        coerce: String
        property :round_id,       coerce: String
        property :type,           coerce: String
        property :index,          coerce: Integer
        property :card,           coerce: String
        property :black_card,     coerce: Common::Boolean
        property :player_cards,   coerce: Array
        property :banker_cards,   coerce: Array
      end
    end
  end
end
