# frozen_string_literal: true

module Simulator
  module Api
    module Responses
      class NotifyBetResponse < Base
        property :action,         coerce: String
        property :feed_id,        coerce: String
        property :round_id,       coerce: String
        property :countdown,      coerce: Integer
      end
    end
  end
end
