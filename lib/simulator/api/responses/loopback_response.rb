# frozen_string_literal: true

module Simulator
  module Api
    module Responses
      # Loop back Api Response
      class LoopbackResponse < Base

        property :name,  coerce: String
      end
    end
  end
end
