# frozen_string_literal: true

module Simulator
  module Api
    module Responses
      class NotifyBurnCardResultResponse < Base
        property :action,         coerce: String
        property :feed_id,        coerce: String
        property :type,           coerce: String
        property :card,           coerce: String
        property :index,          coerce: Integer
        property :remain_count,   coerce: Integer
        property :burn_cards,     coerce: Array
      end
    end
  end
end
