# frozen_string_literal: true

module Simulator
  module Api
    module Responses
      class OpenFeedResultResponse < Base

        class FeedInfo < Base
          property :feed_id,     coerce: String
          property :game_id,     coerce: Integer
          property :state,       coerce: String
          property :config,      coerce: Hash
        end

        property :action,        coerce: String
        property :feed_info,     coerce: FeedInfo
      end
    end
  end
end
