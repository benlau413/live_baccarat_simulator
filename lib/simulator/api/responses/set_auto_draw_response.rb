# frozen_string_literal: true

module Simulator
  module Api
    module Responses
      class SetAutoDrawResponse < Base

        property :auto_draw,  coerce: Common::Boolean
      end
    end
  end
end
