# frozen_string_literal: true

module Simulator
  module Api
    module Responses
      class ShoeInputCardsResponse < Base

        property :action,         coerce: String
        property :shoe_box_id,    coerce: String
        property :shoe,           coerce: Array
      end
    end
  end
end
