# frozen_string_literal: true

module Simulator
  module Api
    module Responses
      # Config Api Response
      class ConfigResponse < Base

        class Config < Common::Base
          property :service,          coerce: Hash
          property :cnfms,            coerce: Hash
          property :gaming_operation, coerce: Hash
        end

        property :config,   coerce: Config
      end
    end
  end
end
