# frozen_string_literal: true

module Simulator
  module Api
    module Responses
      class NotifyResultedResponse < Base

        class History < Base
          property :winner,         coerce: String
          property :pair,           coerce: String
          property :player_point,   coerce: Integer
          property :banker_point,   coerce: Integer
          property :nature,         coerce: String
          property :super6,         coerce: Common::Boolean
        end

        property :action,           coerce: String
        property :feed_id,          coerce: String
        property :shoe_id,          coerce: String
        property :round_id,         coerce: String
        property :player_cards,     coerce: Array
        property :player_point,     coerce: Integer
        property :banker_cards,     coerce: Array
        property :banker_point,     coerce: Integer
        property :winner,           coerce: String
        property :pair,             coerce: String
        property :super6,           coerce: Common::Boolean
        property :history,          coerce: Array[History]
      end
    end
  end
end
