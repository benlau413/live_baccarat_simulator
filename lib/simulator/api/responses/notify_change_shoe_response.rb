# frozen_string_literal: true

module Simulator
  module Api
    module Responses
      class NotifyChangeShoeResponse < Base
        property :action,         coerce: String
        property :feed_id,        coerce: String
        property :shoe_id,        coerce: String
        property :round_id,       coerce: String
        property :hand,           coerce: Integer
      end
    end
  end
end
