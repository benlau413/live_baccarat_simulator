# frozen_string_literal: true

module Simulator
  module Api
    module Responses
      module Common
        # Api Response Common Base Class
        class Base < Hashie::Trash
          include Hashie::Extensions::IgnoreUndeclared
          include Hashie::Extensions::Dash::Coercion
        end
      end
    end
  end
end
