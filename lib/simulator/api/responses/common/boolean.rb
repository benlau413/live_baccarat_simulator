module Simulator
  module Api
    module Responses
      module Common
        class Boolean < Base
          FALSE_VALUES = [false, 0, "0", "false", "FALSE"].to_set
          TRUTH_VALUES = [true, 1, "1", "true","TRUE"].to_set
          def self.coerce(value)
            if FALSE_VALUES.include?(value)
              return false
            elsif TRUTH_VALUES.include?(value)
              return true
            else
              return false
            end
          end
        end
      end
    end
  end
end