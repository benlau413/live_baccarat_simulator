# frozen_string_literal: true

module Simulator
  module Api
    module Responses
      class NotifyNewShoeResponse < Base
        property :action,         coerce: String
        property :feed_id,        coerce: String
      end
    end
  end
end
