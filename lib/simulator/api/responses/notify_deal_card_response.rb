# frozen_string_literal: true

module Simulator
  module Api
    module Responses
      class NotifyDealCardResponse < Base
        property :action,         coerce: String
        property :feed_id,        coerce: String
        property :round_id,       coerce: String
        property :type,           coerce: String
        property :index,          coerce: Integer
        property :card,           coerce: String
        property :kafka_topic,    coerce: String
        property :total_given_card,coerce:Integer
      end
    end
  end
end
