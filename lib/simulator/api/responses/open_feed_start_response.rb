# frozen_string_literal: true

module Simulator
  module Api
    module Responses
      class QueryFeedStartResponse < Base

        property :action,     coerce: String
        property :feed_id,    coerce: String
        property :game_id,    coerce: String
        property :ws_result,  coerce: Common::Boolean
        property :auto_draw,  coerce: Common::Boolean
      end
    end
  end
end
