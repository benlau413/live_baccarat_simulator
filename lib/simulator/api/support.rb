require 'd13n/support/request_id'

module Simulator::Api
  class Service < ::Sinatra::Base

    include ServiceAdapter
    register Sinatra::Namespace

    use Rack::RequestId
    set :public_folder, File.join(File.dirname(__FILE__), *%w[.. .. public])
    set :views, File.join(File.dirname(__FILE__), *%w[.. .. views])
    set :show_exceptions, false
    set :raise_errors, true
    set :logging, true

    before do
      headers 'X-Powered-By' => "Simulator#{Simulator::VERSION}"
      headers 'X-Api-Version' => "#{Simulator::Api::VERSION}"
      D13n::Metric::StreamState.st_get.request_info = request_info
    end

    get "/" do
      "Simulator(#{Simulator::VERSION}:#{Simulator::Api::VERSION}) Say Hi"
    end

    #TODO refactor format handling
    get "/check" do
      @action = 'loopback'
      process(nil, extract_query_string, params.fetch('format', 'json'))
    end

    get "/config" do
      @action = 'config'
      process(nil, extract_query_string, params.fetch('format', 'json'))
    end

    private

    def set_format(value = nil)
      @format = value&.to_sym || :json
    end

    # @param [Hash] params
    # @param [Hash, String] extract_data
    # @param [String] params_format
    # @return [String]
    def process(params, extract_data, params_format)
      set_format(params_format)
      process_message(params, extract_data)
      status respond_status_code
      content_type :json if @format == :json
      respond_with_result
    end

    def extract_message_data
      request.body.read
    end

    def extract_query_string
      Rack::Utils.parse_nested_query(request.query_string) || {}
    end

    def request_info
      {
             "host" => request.host,
               "ip" => request.ip,
       "request_id" => env['HTTP_X_REQUEST_ID'],
           "action" => @action
      }
    end
  end
end
