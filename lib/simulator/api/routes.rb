# frozen_string_literal: true

# load routes stuffs in /routes folder
Dir["#{File.dirname(__FILE__)}/routes/**.rb"]
  .each { |file| require_relative file }
