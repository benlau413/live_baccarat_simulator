# frozen_string_literal: true

require_relative 'messages/utils/validation'
require_relative 'messages/base'
require_relative 'messages/notify_base_message'

# load message stuffs in /messages folder
Dir["#{File.dirname(__FILE__)}/messages/**_message.rb"]
  .each { |file| require_relative file }