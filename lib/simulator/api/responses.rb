# frozen_string_literal: true

require_relative 'responses/base'
require_relative 'responses/common/base'

# load response stuffs in /responses/common folder
Dir["#{File.dirname(__FILE__)}/responses/common/*.rb"]
  .each { |file| require_relative file }

# load response stuffs in /responses folder
Dir["#{File.dirname(__FILE__)}/responses/**_response.rb"]
  .each { |file| require_relative file }
