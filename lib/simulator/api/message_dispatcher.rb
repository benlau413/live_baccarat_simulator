# frozen_string_literal: true

module Simulator
  module Api
    ## Message Dispatcher for dispatch message to correct handler
    class MessageDispatcher
      ## raise HandlerRegisterError when handler don't inherit from Message::Base
      class HandlerRegisterError < Simulator::Errors::UnexpectedError
        def initialize(handler)
          super("[#{handler}] Message Handler Register Error.")
        end
      end

      ## raise HandlerNotFoundError when message isn't dispatcher mapper
      class HandlerNotFoundError < Simulator::Errors::UnexpectedError
        def initialize(handler)
          super("[#{handler}] Message Handler Not Found.")
        end
      end

      class << self
        def instance
          @instance ||= new
        end

        def dispatch(message)
          instance.dispatch(message)
        end
      end

      def initialize
        reset_register
      end

      def dispatch(message)
        handler_name = message.name
        klass = get_handler(handler_name)
        raise HandlerNotFoundError, handler_name if klass.nil?

        klass.dispatch(message)
      end

      def get_handler(msg_name)
        mapper[msg_name.to_sym]
      end

      def reset_register
        register(:loopback,              Simulator::Core::Handlers::LoopbackHandler)
        register(:config,                Simulator::Core::Handlers::ConfigHandler)

        register(:steering_ws_bootup,    Simulator::Core::Handlers::SteeringWsBootupHandler)
        register(:LiveBaccaratMessage,   Simulator::Core::Handlers::LiveBaccaratMessageShoeHandler)
        default_register_handler
      end

      def register(msg_name, handler)
        raise HandlerRegisterError, msg_name unless handler <= Simulator::Core::Handlers::Base

        mapper[msg_name.to_sym] = handler
      end

      private

      def mapper
        @mapper ||= {}
      end

      def default_register_handler
        shoe_register_handler
        dealer_app_register_handler
      end

      def shoe_register_handler
        register(:draw_card,               Simulator::Core::Handlers::ShoeActionHandler)
        register(:lock_shoe,               Simulator::Core::Handlers::ShoeActionHandler)
        register(:unlock_shoe,             Simulator::Core::Handlers::ShoeActionHandler)

        register(:shoe_draw,               Simulator::Core::Handlers::ShoeDrawHandler)
        register(:shoe_input_cards,        Simulator::Core::Handlers::ShoeInputCardsHandler)
        register(:shoe_clean_cards,        Simulator::Core::Handlers::ShoeCleanCardsHandler)
        register(:shoe_show_cards,         Simulator::Core::Handlers::ShoeShowCardsHandler)
        register(:shoe_show_all,           Simulator::Core::Handlers::ShoeShowAllHandler)
        register(:set_auto_draw,           Simulator::Core::Handlers::SetAutoDrawHandler)
      end

      def dealer_app_register_handler
        register(:query_feed_start,               Simulator::Core::Handlers::QueryFeedStartHandler)
        # register(:open_feed,                     Simulator::Core::Handlers::NotifyOpenFeedHandler)

        register(:notify_change_shoe,            Simulator::Core::Handlers::NotifyChangeShoeHandler)
        register(:notify_shoe_locked,            Simulator::Core::Handlers::NotifyHandler)
        register(:notify_shoe_unlocked,          Simulator::Core::Handlers::NotifyHandler)
        register(:notify_new_shoe,               Simulator::Core::Handlers::NotifyHandler)
        register(:notify_burn_card,              Simulator::Core::Handlers::NotifyDrawCardHandler)
        register(:notify_burn_card_result,       Simulator::Core::Handlers::NotifyDrawCardResultHandler)
        register(:notify_bet_started,            Simulator::Core::Handlers::NotifyHandler)
        register(:notify_bet_closing,            Simulator::Core::Handlers::NotifyHandler)
        register(:notify_bet_closed,             Simulator::Core::Handlers::NotifyHandler)
        register(:notify_deal_card,              Simulator::Core::Handlers::NotifyDrawCardHandler)
        register(:notify_deal_card_result,       Simulator::Core::Handlers::NotifyDrawCardResultHandler)
        register(:notify_resulted,               Simulator::Core::Handlers::NotifyHandler)
        register(:notify_closed,                 Simulator::Core::Handlers::NotifyHandler)
      end
    end
  end
end
