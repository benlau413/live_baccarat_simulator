# frozen_string_literal: true

module Simulator
  module Api
    ## Provide Interface for various web framework
    module ServiceAdapter
      def self.included(descendant)
        descendant.extend ClassMethods
        descendant.class_eval do
          include Simulator::Utils::Serializer
          include InstanceMethods
        end
      end

      ## Instance Method Module For ServiceAdapter
      module InstanceMethods
        attr_reader :context

        private

        def init
          @context = Context.new
        end

        # @param [Hash] params
        # @param [Hash, String] extract_data
        def before_update(params, extract_data)
          parse_params(params)
          parse_extract_data(extract_data)
          build_message
        end

        def update
          context.result = MessageDispatcher.dispatch(context.message)
        end

        # @param [Hash] params
        # @param [Hash, String] extract_data
        def process_message(params, extract_data)
          @started_at = Time.now
          begin
            init
            before_update(params, extract_data)
            update
            after_update
          rescue StandardError => ex
            Simulator.log_exception(ex)
            context.result = ex
          ensure
            final
          end
        end

        def after_update; end

        def final
          @processed_at = Time.now
        end

        def parse_params(params)
          request_params = params.is_a?(Hash) ? params : {}
          context.update_request_data(request_info)
          context.update_request_data(request_params)
        end

        def parse_extract_data(extract_data)
          extract_params = extract_data.is_a?(Hash)   ? extract_data :
                           extract_data.is_a?(String) ?
                           extract_data.empty? ? {} : deserialize_value(extract_data) : {}
          context.update_request_data(extract_params)
        end

        def build_message
          context.message = MessageFactory.compose(context.request_data)
        end

        def request_info
          raise NotImplementedError
        end

        def respond_status_code
          context.status
        end

        def respond_with_result
          response = context.response
          result = serialize_value(response)
          @responded_at = Time.now
          Simulator.logger.info(
            action: @action,
            in:  context.request_data.to_s,
            out: result,
            pt: ((@processed_at - @started_at) * 1000).round, # message processing time
            rt: ((@responded_at - @processed_at) * 1000).round, # message response time
            tt: ((@responded_at - @started_at) * 1000).round # message total process time
          )
          result
        end
      end

      ## Class Method Module For ServiceAdapter
      module ClassMethods
      end
    end
  end
end
