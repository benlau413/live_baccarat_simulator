# frozen_string_literal: true

module Simulator
  module Api
    ## request context include input/output
    class Context
      attr_accessor :message
      attr_reader   :request_data, :result
      Error = Simulator::Errors::UnexpectedError

      def initialize
        @result = nil
        @request_data = @message = {}
      end

      def result=(ctx)
        @result = case ctx
                  when Hash, StandardError, nil
                    ctx
                  else
                    Error.new("API Context #{ctx} should be Hash or StandardError")
                  end
      end

      def update_request_data(data)
        @request_data.merge!(data) { |_key, old_value, _new_value| old_value }
      end

      def status
        case result
        when StandardError, nil ## rasie error case and nothing response case
          lookup_http_error_code(result)
        else ## normal case
          200
        end
      end

      def ok?
        status == 200
      end

      def response
        case result
        when StandardError, nil ## rasie error case and nothing response case
          response_error
        else ## normal case
          response_result
        end
      end

      private

      def response_result
        ResponseManager.represent(message, result)
      rescue StandardError => ex
        Simulator.log_exception ex
        @result = Error.new(ex.message)
        response
      end

      def response_error
        result.respond_to?(:to_hash) ? result.to_hash : Error.new(result&.message).to_hash
      end

      def lookup_http_error_code(error)
        # #loop sequence in default_http_error_code
        default_http_error_code.each do |error_klass, http_error_code|
          return http_error_code if error.class <= Simulator::Errors.const_get(error_klass)
        end
        500
      end

      def default_http_error_code
        # child error code should be front
        {
          'ClientError' => 400,
          'UnexpectedError' => 500
        }
      end
    end
  end
end
