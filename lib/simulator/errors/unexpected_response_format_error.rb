# frozen_string_literal: true

module Simulator
  module Errors
    # Unexpected Response Format Error
    # raise error when remote call get unexpected format
    class UnexpectedResponseFormatError < UnexpectedError
      def initialize(message = 'Unexpected Response Format Error')
        super(message)
      end

      def code
        'S3'
      end
    end
  end
end
