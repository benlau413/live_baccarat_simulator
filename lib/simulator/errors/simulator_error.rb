# frozen_string_literal: true

module Simulator
  module Errors
    # Base Error For Simulator
    class SimulatorError < StandardError
      attr_reader :description

      def initialize(description = 'Simulator Error')
        @description = description
        super(@description)
      end

      def problem
        self.class.name.underscore.split('/').last
      end

      def resolution
        ''
      end

      def code; end

      def to_hash
        {
          code: code,
          error_code: problem,
          description: description,
          resolution: resolution
        }
      end
    end
  end
end
