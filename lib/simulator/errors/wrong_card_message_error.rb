# frozen_string_literal: true

module Simulator
  module Errors
    class WrongCardMessageError < ClientError
      def initialize(message = 'Wrong Card Message Error')
        super(message)
      end

      def code
        'S4'
      end

      def resolution
        'Please input the correct card message.'
      end
    end
  end
end
