# frozen_string_literal: true

module Simulator
  module Errors
    class CardExceedLimitError < ClientError
      def initialize(message = 'Card Exceed Limit Error')
        super(message)
      end

      def code
        'S5'
      end

      def resolution
        'Please input another card.'
      end
    end
  end
end
