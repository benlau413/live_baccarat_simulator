# frozen_string_literal: true

module Simulator
  module Errors
    # Unexpected Error
    # same as Internal Service Error
    class UnexpectedError < SimulatorError
      def initialize(description = nil)
        description ||= 'Unexpected Error'
        super(description)
      end

      def resolution
        'The service happen unexpected error.'
      end

      def code
        'S0'
      end
    end
  end
end
