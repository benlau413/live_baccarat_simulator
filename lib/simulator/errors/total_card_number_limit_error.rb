# frozen_string_literal: true

module Simulator
  module Errors
    class TotalCardNumberLimitError < ClientError
      def initialize(message = 'Total Card Number Limit Error')
        super(message)
      end

      def code
        'S6'
      end

      def resolution
        'Please change a new shoe.'
      end
    end
  end
end
