# frozen_string_literal: true

module Simulator
  module Errors
    # Parameter Mismatch Error
    # raise error when request parameter mismatch
    class ParameterMismatchError < ClientError
      def initialize(message = 'Parameter validation error')
        super(message)
      end

      def code
        'S2'
      end

      def resolution
        'Please double confirm API spec.'
      end
    end
  end
end
