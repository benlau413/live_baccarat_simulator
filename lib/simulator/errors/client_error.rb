# frozen_string_literal: true

module Simulator
  module Errors
    # Client Error
    # request have some issue
    class ClientError < SimulatorError
      def initialize(description = nil)
        description ||= 'Client Error'
        super(description)
      end

      def resolution
        'The server cannot or will not process the request due to an apparent client error.'
      end

      def code
        'S1'
      end
    end
  end
end
