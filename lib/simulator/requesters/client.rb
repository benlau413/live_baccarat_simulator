# frozen_string_literal: true

require 'http'
module Simulator
  module Requesters
    # Client
    class Client
      DEFAULT_TIMEOUT = 3 # sec

      class << self
        # Post to a response
        # @params url String
        # @option params [Hash]
        # @return [HTTP::Response]
        def post(url:, params: nil, type: :json)
          perform(:post, url, type => params)
        end

        # Get to a response
        # @params url String
        # @option params [Hash]
        # @return [HTTP::Response]
        def get(url:, params: nil)
          perform(:get, url, params: params)
        end

        protected

        # @return [HTTP::Response]
        def perform(verb, url, opts = {})
          st = Time.now
          response = HTTP.timeout(DEFAULT_TIMEOUT).request verb, url, opts
          Simulator.logger.info(
            type: 'http',
            verb: verb,
            url: url,
            opts: opts.to_s,
            response: response.body.to_s.force_encoding('UTF-8'),
            tt: ((Time.now - st) * 1000).round
          )
          response
        rescue HTTP::TimeoutError, HTTP::ConnectionError
          raise Simulator::Errors::ConnectionError, \
                "call #{url} with #{opts} timeout."
        rescue StandardError => ex
          raise Simulator::Errors::UnexpectedError, ex.message
        end
      end
    end
  end
end
