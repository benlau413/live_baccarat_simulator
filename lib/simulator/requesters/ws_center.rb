# frozen_string_literal: true

module Simulator::Requesters
  module WsCenter
    extend self

    class << self
      def ws_dealer_client
        $ws_dealer_client ||= []
      end

      def steering_bootup(params)
        port = ENV['port'] || Simulator.config[:port]
        @steering_ws_host = "ws://#{Simulator.config[:'service.steering.host']}"
        puts @steering_ws_host
        @steering_url ||= @steering_ws_host + format(Simulator.config[:'service.steering.path'], game_id: params[:game_id], feed_id: params[:feed_id])
        @ws_steering ||= Faye::WebSocket::Client.new(@steering_url)
        thr = Thread.new do
          @ws_steering.on :message do |event|
            Simulator.logger.info "Steering Client message received(#{event.data.size}) #{event.data}"
            action = get_action(event.data)
            Simulator.logger.info("action_name: #{action}")
            unless action.nil?
              @ws_local ||= Faye::WebSocket::Client.new(format("ws://0.0.0.0:%{port}/ws", port: port))
              @ws_local.send(event.data)
            end
          end
        end
        thr.join
        true
      end

      def steering_call(params)
        steering_bootup(params) if @ws_steering.nil?
        send_response = @ws_steering.send(MultiJson.dump(params))
      end

      def dealer_client_call(params)
        return if ws_dealer_client.empty?
        Simulator.logger.info("ws_dealer_client = #{ws_dealer_client}")
        ws_dealer_client.each do |client|
          send_response = client.send(MultiJson.dump(params))
        end
      end
    end

    def get_action(event_data)
      event_data_hash = MultiJson.load(event_data)
      action = event_data_hash["action"]
    end
  end
end
