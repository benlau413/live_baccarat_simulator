# frozen_string_literal: true

module Simulator::Requesters
  module MessageCenter
    extend self

    def bootup(shoe_box_id)
      Simulator.logger.info("Kafka host: #{Simulator.config[:'service.kafka.host']}")
      brokers = Simulator.config[:'service.kafka.host']
        # brokers = '0.0.0.0:9092'
      client_id = "simulator_" + shoe_box_id
      @kafka = Kafka.new(brokers, client_id: client_id, logger: Simulator.logger)
    end

    def kafka_produce_message(kafka_message)
      bootup(kafka_message[:shoe_box_id]) if @kafka.nil?
      @producer = @kafka.async_producer
      topic = construct_topic(kafka_message[:shoe_box_id])
      @producer.produce(
        MultiJson.dump(kafka_message),
        topic: topic
      )
      @producer.deliver_messages
      @producer.shutdown
      Simulator.logger.info('Kafka message: ' + MultiJson.dump(kafka_message))
      kafka_info = {
        kafka_host: Simulator.config[:'service.kafka.host'],
        topic: topic
      }
    end

    def construct_topic(topic)
      "#{Simulator.config[:'idc.codename']}_#{topic}"
    end
  end
end
