# frozen_string_literal: true

module Simulator
  module Requesters
    # Base
    class Base
      class << self
        def register(name, extension)
          klass_name = name.to_s.camelize + 'Request'
          const_set klass_name, create_class(extension)
          define_class_method(name, klass_name)
        end

        def call(**params)
          new.call(params)
        end

        protected

        def create_class(extension)
          Class.new(self) do
            include extension
            define_method(:call) do |**params|
              request_params = build_request_params(params)
              res = Client.send(method, url: url, params: request_params)
              extract_response(res)
            end
          end
        end

        def define_class_method(name, klass_name)
          instance_eval <<-RUBY, __FILE__, __LINE__ + 1
            def #{name}(**params)
              #{self.name}::#{klass_name}.call(params)
            end
          RUBY
        end
      end

      protected

      def url
        base_url + path
      end

      %w[base_url path method
         build_request_params extract_response].each do |name|
        define_method(name) do
          raise NotImplementedError, \
                "#{self.class.name}##{__method__} is an abstract method."
        end
      end

      def parse_json_response(response)
        MultiJson.load(
          response.body.to_s,
          symbolize_keys: true
        )
      rescue StandardError
        raise Errors::UnexpectedResponseFormatError
      end
    end
  end
end
