# frozen_string_literal: true

module Simulator
  module Utils
    ## Provide Default Serializer && Deserializer
    ## can use register_format to register new serializer && deserializer
    ## to support more format
    module Serializer
      extend self
      REGISTERED_FORMATS = {}

      class << self
        def extended(descendant)
          descendant.extend CommonMethods
          descendant.instance_variable_set('@format', :json)
        end

        def included(descendant)
          descendant.include CommonMethods
          descendant.instance_variable_set('@format', :json)
        end
      end

      def register_format(format, serializer, deserializer)
        REGISTERED_FORMATS[format.to_sym] = [serializer, deserializer]
      end

      def register_formats
        REGISTERED_FORMATS
      end

      register_format(:yaml, ->(value) { value.to_yaml }, ->(value) { YAML.load(value) })
      register_format(:json, ->(value) { MultiJson.dump(value) }, ->(value) { MultiJson.load(value) })

      ## CommonMethods For Serializer
      ## Provide serializer && deserializer
      module CommonMethods
        attr_accessor :format

        def serialize_value(value)
          serializer.call(value)
        end

        def deserialize_value(value)
          deserializer.call(value)
        end

        def serializer
          serializer, deserializer = register_formats[format]
          serializer
        end

        def deserializer
          serializer, deserializer = register_formats[format]
          deserializer
        end
      end
    end
  end
end
