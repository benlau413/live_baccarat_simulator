# frozen_string_literal: true

module Simulator::Utils
  module MachineToken
    # #property_id | zone_id | zone_name | location_id | location_name | machine_id | machine_name | uuid | casino_id
    def get_machine_name(machine_token)
      machine_token.split('|')[6]
    end

    def get_zone_name(machine_token)
      machine_token.split('|')[2]
    end

    def get_location_name(machine_token)
      machine_token.split('|')[4]
    end

    def get_property_id(machine_token)
      machine_token.split('|')[0]
    end
  end
end
