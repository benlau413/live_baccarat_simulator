# frozen_string_literal: true

require 'bigdecimal'

module Simulator
  module Utils
    # Common AmtUtil Methods
    module AmtUtil
      def to_cents_i(amt)
        to_cents(amt).to_i
      end

      def to_dollar_str(amt)
        format("%.2f", to_dollar(amt))
      end

      def to_cents(amt)
        BigDecimal(amt) * BigDecimal('100.0')
      end

      def to_dollar(amt)
        BigDecimal(amt) * BigDecimal('0.01')
      end
    end
  end
end
