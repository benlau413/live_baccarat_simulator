# frozen_string_literal: true

require_relative 'requesters/client'
require_relative 'requesters/base'
require_relative 'requesters/message_center'
require_relative 'requesters/ws_center'

# load requesters code stuffs in /requesters folder
Dir["#{File.dirname(__FILE__)}/requesters/**.rb"]
  .each { |file| require_relative file }
