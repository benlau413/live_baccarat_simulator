# frozen_string_literal: true

# load util code stuffs in /utils folder
Dir["#{File.dirname(__FILE__)}/utils/**.rb"]
  .each { |file| require_relative file }
