require 'd13n'
require 'simulator/version'
require 'dry-struct'
require 'multi_json'
require 'stackprof'
require 'hashie'
require 'timezone'
require 'gaming-random-kit'
require 'recursive-open-struct'

require 'kafka'

module Types
  include Dry.Types
end

module Simulator
  extend D13n::Application::ClassMethods
  def self.log_exception(err)
    case err
    when StandardError
      logger.error(
        "error_class: #{err.class}" + "\n" + err.message + "\n" + err.backtrace[0..15].join("\n")
      )
    else
      logger.error err
    end
  end

  # D13n defined logger, config and config's default configuration 'default_source' in Simulator, you can use
  # Simulator.config=
  # Simulator.logger=
  # to assign another configurator or logger.

  Simulator.default_source = {
    support_service_type: {
      default: %w[
        http_shoe_api
        http_dealer_api
        ws_api
        dealer_client_api
      ],
      public: true,
      allowed_from_server: true,
      type: Array,
      description: 'Simulator Service Type.'
    },
    'service.shoe.base_path': {
      default: '/shoe',
      public: true,
      type: String,
      description: 'Defines base path for shoe api endpoint.'
    },

    'service.dealer.base_path': {
      default: '/dealer',
      public: true,
      type: String,
      description: 'Defines base path for dealer_app api endpoint.'
    },

    # Kafka Config
    'service.kafka.host': {
      default: ["sandslash-vapp01.int.ias.local:10001", "sandslash-vapp02.int.ias.local:10002", "sandslash-vapp03.int.ias.local:10003"],
      # default: '0.0.0.0:9092',
      public: true,
      type: Array,
      allowed_from_server: true,
      description: 'Kafka broker host.',
    },
    'idc.codename': {
      default: 'test',
      public: true,
      type: String,
      allowed_from_server: true,
      description: 'idc codename.',
    },

    # Steering Config
    'service.steering.host': {
      default: '10.10.5.135:4000',
      # default: '0.0.0.0:5555',
      public: true,
      type: String,
      allowed_from_server: true,
      description: 'Steering Service host.',
    },

    'service.steering.path': {
      default: '/external/games/%{game_id}/feeds/%{feed_id}',
      public: true,
      type: String,
      allowed_from_server: true,
      description: 'Steering Service path.',
    },
  }
end

require 'simulator/errors'
require 'simulator/utils'
require 'simulator/requesters'
require 'simulator/api'
require 'simulator/core'
require 'simulator/service'
